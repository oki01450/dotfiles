bind "TAB:menu-complete"
bind "set show-all-if-ambiguous on"
bind "set completion-ignore-case on"

export PS1="\[\033[34m\]\W > \[$(tput sgr0)\]"
# set cursor to ibeam
export PS1="\[\e[5 q\r\]$PS1"

alias grep="grep --color=auto"
alias ls="ls --color=auto"
